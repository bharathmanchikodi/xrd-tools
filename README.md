# XRD Tools
A few simple tools for XRD analysis

1. dat2tsv - Convert FullProf dat files to TSV
2. DebyeFit - Fit Debye function to unit cell volume
3. QPlotr2020 - Plot Rietveld PRF files using PyQt5 and PyQtGraph
4. raw2dat - Convert Rigaku RAS file to FullProf dat format
5. raw2tsv - Convert Rigaku RAS file to TSV file
6. tsv2dat - Convert TSV to FullProf dat format
